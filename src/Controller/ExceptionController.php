<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ExceptionController
{

    public function error(Throwable $exception)
    {
        $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        if(method_exists($exception, 'getStatusCode') && !empty($exception->getStatusCode())) {
            $status = $exception->getStatusCode();
        } elseif(!empty($exception->getCode())) {
            $status = $exception->getCode();
        }
        return new JsonResponse([
            'message' => $exception->getMessage()
        ], $status);
    }

}
