<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

class ApiController extends AbstractController
{

    /**
     * @Route("/", name="index")
     * @throws Exception
     */
    public function index(): Response
    {
        return $this->redirect('/api/users');
    }

    /**
     * @Route("/api/users/{id}", name="get_user", methods={"GET"})
     * @throws Exception
     */
    public function getUser(int $id, UserRepository $ur): Response
    {
        $user = $ur->findOneArray('id', $id);
        if(!$user) {
            throw new Exception('User with this ID was not found', Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse([
            'data' => $user[0],
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/api/users", name="get_users", methods={"GET"})
     * @throws Exception
     */
    public function getUsers(UserRepository $ur, Request $request): Response
    {
        $limit = !empty($request->get('per_page')) && (int) $request->get('per_page') > 0 ? (int) $request->get('per_page') : 10;
        $offset = !empty($request->get('page')) ? ((int) $request->get('page')) * $limit : 0;
        $users = $ur->findAllArray($offset, $limit);
        if(!$users) {
            throw new Exception('No users found', Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse([
            'page' => $offset,
            'per_page' => $limit,
            'users' => $users,
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/api/users/{id}", name="delete_user", methods={"DELETE"})
     * @throws Exception
     */
    public function deleteUser(int $id, UserRepository $ur, ManagerRegistry $doctrine): Response
    {
        $user = $ur->findOneBy(['id' => $id]);
        if(!$user) {
            throw new Exception('User with this ID was not found', Response::HTTP_NOT_FOUND);
        }
        $em = $doctrine->getManager();
        $em->remove($user);
        $em->flush();
        return new JsonResponse([
            'message' => 'User deleted',
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/api/users", name="add_user", methods={"POST"})
     * @throws Exception
     */
    public function addUser(UserRepository $ur, ManagerRegistry $doctrine, Request $request): Response
    {
        try {
            if (!$request->get('email') || !$request->get('first_name') || !$request->get('last_name')) {
                throw new Exception('You must complete all fields', Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $check = $ur->checkRepeat($request->get('email'));
            if ($check['status']) {
                $user = new User();
                $user->setEmail($request->get('email'));
                $user->setFirstName($request->get('first_name'));
                $user->setLastName($request->get('last_name'));
                $user->setAvatar($request->get('avatar'));

                $em = $doctrine->getManager();
                $em->persist($user);

                $em->flush();

                $user = $ur->findOneArray('id', $user->getId());

                if ($user && !empty($user[0])) {
                    return new JsonResponse([
                        'added_at' => time(),
                        'user' => $user[0]
                    ], Response::HTTP_CREATED);
                }
            } else {
                return new JsonResponse([
                    'message' => $check['message']
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            throw new Exception('A problem occured! Please try again in a moment.', Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $a) {
            return new JsonResponse([
                'message' => 'A problem occured! Please try again in a moment.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/api/users/{id}", name="update_user", methods={"PATCH", "PUT"})
     * @throws Exception
     */
    public function updateUser(UserRepository $ur, ManagerRegistry $doctrine, Request $request, int $id): Response
    {
        $user = $ur->findOneBy(['id' => $id]);
        if(!$user) {
            throw new Exception('No user with the given id was found', Response::HTTP_NOT_FOUND);
        }
        if($request->getMethod() == 'PATCH') {
            $user = $ur->updateByPATCH($user, $request, $id);
        } elseif($request->getMethod() == 'PUT') {
            $user = $ur->updateByPUT($user, $request, $id);
        }

        $em = $doctrine->getManager();
        $em->persist($user);
        $em->flush();

        $user = $ur->findOneArray('id', $user->getId());

        if($user && !empty($user[0])) {
            return new JsonResponse([
                'message' => 'User updated',
                'user' => $user[0],
                'updated_at' => time()
            ], Response::HTTP_OK);
        }
        return new JsonResponse([
            'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
