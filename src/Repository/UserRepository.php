<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findOneArray(string $key, $value)
    {
        return $this->createQueryBuilder('u')
            ->where('u.' . $key . ' = :value')
            ->setParameter('value', $value)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function findAllArray(int $offset = 0, int $limit = 10)
    {
        return $this->createQueryBuilder('u')
            ->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * @throws Exception
     * check if provided email is already taken
     */
    public function checkRepeat(string $email, int $id = null): Array
    {
        $query =  $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email);
        if(!empty($id)) {
            $query = $query->andWhere('u.id != :id')
                ->setParameter('id', $id);
        }
        $result = $query->getQuery()
            ->getResult();
        if (!empty($result)) {
            return ['status' => false, 'message' => 'The given email address is already taken'];
        }
        return ['status' => true];
    }

    public function updateByPATCH(User $user, Request $request, int $id)
    {
        if ($request->get('email')) {
            $check = $this->checkRepeat($request->get('email'), $id);
            if($check['status']) {
                $user->setEmail($request->get('email'));
            } else {
                throw new Exception($check['message'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }
        if ($request->get('first_name')) {
            $user->setFirstName($request->get('first_name'));
        }
        if ($request->get('last_name')) {
            $user->setLastName($request->get('last_name'));
        }
        if ($request->get('avatar')) {
            $user->setAvatar($request->get('avatar'));
        }
        return $user;
    }

    public function updateByPUT(User $user, Request $request, int $id)
    {
        if ($request->get('email') && $request->get('first_name') && $request->get('last_name')) {
            $check = $this->checkRepeat($request->get('email'), $id);
            if($check['status']) {
                $user->setEmail($request->get('email'));
                $user->setFirstName($request->get('first_name'));
                $user->setLastName($request->get('last_name'));
                if($request->get('avatar')) {
                    $user->setAvatar($request->get('avatar'));
                }
            } else {
                throw new Exception($check['message'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } else {
            throw new Exception('You must complete all fields', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return $user;
    }

}
