<?php
namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

class DownloadUsersCommand extends Command
{
    protected static $defaultName = 'app:download-users';
    protected $url = 'https://reqres.in/api/users?page=2';
    protected $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setHelp('This command allows you to download users from defined url and save them to the database.');
    }

    protected function downloadUsers(): Array
    {
        try {
            $httpClient = HttpClient::create();
            $response = $httpClient->request('GET', $this->url);
            if ($response->getStatusCode() === 200) {
                $content = json_decode($response->getContent());
                if(is_object($content)) {
                    $users = $content->data;
                    return $this->batchInsert($users);
                }
            }
            return ['status' => false, 'message' => 'A problem occured! The page with the given url returns an error.'];
        } catch (\Exception $a) {
            return ['status' => false, 'message' => 'A problem occured! ' . $a->getMessage()];
        }
    }

    public function batchInsert(Array $data): Array
    {
        try {
            $em = $this->doctrine->getManager();
            $batchSize = 20;

            foreach ($data as $i => $userData) {
                $user = new User();
                $user->setAll($userData);
                $em->persist($user);
                if (($i % $batchSize) === 0) {
                    $em->flush();
                    $em->clear();
                }
            }
            $em->flush();
            $em->clear();

            return ['status' => true, 'message' => 'Users downloaded successfully'];
        } catch (\Exception $a) {
            return ['status' => false, 'message' => $a->getMessage()];
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $download = $this->downloadUsers();
        $output->write('> ' . $download['message']);
        if($download['status']) {
            return Command::SUCCESS;
        }
        return Command::FAILURE;
    }
}