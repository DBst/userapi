CREATE TABLE
    user (
        id INT AUTO_INCREMENT NOT NULL,
        email VARCHAR(150) NOT NULL,
        first_name VARCHAR(50) NOT NULL,
        last_name VARCHAR(80) NOT NULL,
        avatar VARCHAR(255) DEFAULT NULL,
        PRIMARY KEY(id)
     ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB